import UIKit

class SizeController {
    static let originalSize = CGSize(width: 1080, height: 1920)

    var defaultInset: CGFloat {
        return mainSize.height * 0.05
    }

    let mainSize: CGSize

    init(availableSize: CGSize) {
        mainSize = SizeController.calculatedSize(availableSize: availableSize, originalSize: SizeController.originalSize)
    }

    private class func calculatedSize(availableSize: CGSize, originalSize: CGSize) -> CGSize {
        let ratio = originalSize.height / originalSize.width
        if availableSize.height > availableSize.width {
            return CGSize(
                width: availableSize.width,
                height: availableSize.width * ratio
            )
        } else {
            return CGSize(
                width: availableSize.height / ratio,
                height: availableSize.height
            )
        }
    }
}
