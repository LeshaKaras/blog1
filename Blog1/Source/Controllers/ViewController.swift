import SnapKit
import UIKit

let girlImage = UIImage(named: "girlImage")!
let backgroundImage = UIImage(named: "backgroundImage")!

class ViewController: UIViewController {
    // MARK: Private properties

    private var availableView = UIView()

    private var backgroundImageView: UIImageView {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.image = backgroundImage
        imageView.layer.cornerRadius = 8
        return imageView
    }

    // MARK: Overrides

    @IBAction func reloadAction(_ sender: UIButton) {
        availableView.subviews.forEach {
            $0.removeFromSuperview()
            $0.removeConstraints()
        }
        start()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        start()
    }

    // MARK: Private methods

    private func setupViews() {
        view.backgroundColor = .black
        view.addSubview(availableView) {
            $0.center.equalToSuperview()
            $0.size.equalToSuperview().multipliedBy(0.8) // изменить multipliedBy (0.5 / 0.3) для теста отрисовки layer
        }
    }

    private func start() {
        let sizeController = SizeController(availableSize: availableView.bounds.size)
        let backgroundView = setupMainBackgroundView(on: availableView, with: sizeController)
        let blog1Layer = setupAnimateLayer(on: backgroundView.layer, with: sizeController)
        blog1Layer.animate()
    }

    private func setupMainBackgroundView(on view: UIView, with sizeController: SizeController) -> UIView {
        let backgroundView = backgroundImageView
        view.addSubview(backgroundView) {
            $0.center.equalToSuperview()
            $0.size.equalTo(sizeController.mainSize)
        }
        view.layoutIfNeeded()
        return backgroundView
    }

    private func setupAnimateLayer(on layer: CALayer, with sizeController: SizeController) -> AnimateLayerProtocol {
        let mainLayer: AnimateLayerProtocol = MainLayer(
            setupModel: setupSetupMainLayerModelWith(controller: sizeController)
        )
        layer.addSublayer(mainLayer)
        mainLayer.setupFrame(cgRect: layer.bounds)
        return mainLayer
    }

    // Тайминги анимации, если нужно описать более подробно - не проблема =) Просто скажите что интересует
    private func setupSetupMainLayerModelWith(controller: SizeController) -> SetupAnimationLayerProtocol {
        return DefaultSetupModel(
            defaultInset: controller.defaultInset,
            topTimings: TopLayerAnimationTimings(
                deploymentDuration: 1,
                zoomDuration: 2,
                zoomSetting: ZoomSettingModel(fromValue: 1.1, toValue: 1)
            ),
            bottomTimings: BottomLayerAnimationTimings(
                firstStageDuration: 0.7,
                secondStageDuration: 0.6
            )
        )
    }
}
