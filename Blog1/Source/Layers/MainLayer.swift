import UIKit

class MainLayer: CALayer, AnimateLayerProtocol {
    private let topLayer: AnimateLayerProtocol
    private let bottomLayer: AnimateLayerProtocol

    let defaultInset: CGFloat

    init(setupModel: SetupAnimationLayerProtocol) {
        defaultInset = setupModel.defaultInset
        topLayer = TopLayer(defaultInset: setupModel.defaultInset, timings: setupModel.topTimings)
        bottomLayer = BottomLayer(defaultInset: setupModel.defaultInset, timings: setupModel.bottomTimings)
        
        super.init()
        setupSubLayers()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupFrame(cgRect: CGRect) {
        frame = cgRect
        topLayer.setupFrame(
            cgRect: CGRect(
                x: bounds.minX + defaultInset,
                y: bounds.minY,
                width: bounds.width - (defaultInset * 2),
                height: bounds.height / 3 * 2
            )
        )
        bottomLayer.setupFrame(
            cgRect: CGRect(
                x: bounds.minX,
                y: topLayer.frame.maxY,
                width: bounds.width,
                height: bounds.height / 3
            )
        )
    }

    func animate() {
        topLayer.animate()
        bottomLayer.animate()
    }

    private func setupSubLayers() {
        addSublayer(topLayer)
        addSublayer(bottomLayer)
    }
}
