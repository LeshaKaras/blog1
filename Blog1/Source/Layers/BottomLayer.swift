import UIKit

class BottomLayer: CALayer, AnimateLayerProtocol {
    private let whiteLayer = CALayer()
    private let pinkLayer = CALayer()
    private let textContentLayer = CALayer()

    private let titleTextLayer: CATextLayer = {
        let layer = CATextLayer()
        layer.contentsScale = UIScreen.main.scale
        return layer
    }()

    private let infoTextLayer: CATextLayer = {
        let layer = CATextLayer()
        layer.contentsScale = UIScreen.main.scale
        return layer
    }()

    private var defaultTextContentLayerHeight: CGFloat {
        bounds.height / 3
    }

    private var sprawlingPinkLayerHeight: CGFloat {
        bounds.height / 2.5
    }

    private let additionalFirstStageDelay: Double = 0.2

    private let defaultInset: CGFloat
    private let timings: BottomLayerAnimationTimings

    init(
        defaultInset: CGFloat,
        timings: BottomLayerAnimationTimings
    ) {
        self.defaultInset = defaultInset
        self.timings = timings
        super.init()
        setupSubLayers()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupFrame(cgRect: CGRect) {
        frame = cgRect
        whiteLayer.frame = CGRect(
            x: bounds.minX,
            y: bounds.minY,
            width: bounds.width,
            height: 0
        )
        whiteLayer.position = .zero
        pinkLayer.frame = CGRect(
            x: bounds.minX,
            y: bounds.minY,
            width: bounds.width,
            height: 0
        )
        pinkLayer.position = .zero
    }

    func animate() {
        doFirstStageAnimation()
    }

    // MARK: Animations

    private func doFirstStageAnimation() {
        let whiteLayerAnimation = CABasicAnimation.transfromLayerBounds(
            whiteLayer,
            newHeight: bounds.height,
            duration: timings.firstStageDuration,
            delay: 0
        )
        let pinkLayerAnimation = CABasicAnimation.transfromLayerBounds(
            pinkLayer,
            newHeight: bounds.height,
            duration: timings.firstStageDuration,
            delay: additionalFirstStageDelay
        )
        pinkLayer.bounds = CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: bounds.width, height: bounds.height))

        CATransaction.begin()
        CATransaction.setCompletionBlock { [weak self] in
            self?.doSecondStageAnimation()
        }
        whiteLayer.add(whiteLayerAnimation, forKey: "bounds")
        pinkLayer.add(pinkLayerAnimation, forKey: "firstAnimationKey")
        CATransaction.commit()
    }

    private func doSecondStageAnimation() {
        func animatePinkLayer() {
            let newBounds = CGRect(
                x: bounds.minX + defaultInset,
                y: bounds.minY,
                width: bounds.width - (defaultInset * 2),
                height: sprawlingPinkLayerHeight
            )

            let boundsAnimation = CABasicAnimation(keyPath: "bounds")
            boundsAnimation.fromValue = NSValue(cgRect: pinkLayer.bounds)
            boundsAnimation.toValue = NSValue(cgRect: newBounds)

            let positionAnimation = CABasicAnimation(keyPath: "position")
            positionAnimation.fromValue = NSValue(cgPoint: CGPoint(x: bounds.minX, y: bounds.minY))
            positionAnimation.toValue = NSValue(cgPoint: CGPoint(x: newBounds.minX, y: newBounds.minY))

            let group = CAAnimationGroup()
            group.fillMode = .both
            group.isRemovedOnCompletion = false
            group.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
            group.duration = CFTimeInterval() + timings.secondStageDuration
            group.animations = [boundsAnimation, positionAnimation]
            pinkLayer.bounds = newBounds

            CATransaction.begin()
            CATransaction.setCompletionBlock { [weak self] in
                self?.animateTitleLayer()
            }
            pinkLayer.add(group, forKey: "boundsAnimationAndpositionAnimation")
            CATransaction.commit()
        }

        func animateContentTextLayer() {
            pinkLayer.addSublayer(textContentLayer)
            textContentLayer.backgroundColor = UIColor.white.cgColor

            let newX = pinkLayer.bounds.midX
            let newY = pinkLayer.bounds.minY - defaultInset
            let newHeight: CGFloat = defaultTextContentLayerHeight
            let newWidht = pinkLayer.bounds.width - (defaultInset * 2)

            textContentLayer.frame = CGRect(
                x: newX,
                y: newY,
                width: 0,
                height: newHeight
            )

            let animation = CABasicAnimation.transfromLayerBounds(
                textContentLayer,
                newWidht: newWidht,
                duration: timings.secondStageDuration,
                delay: 0
            )

            textContentLayer.bounds = CGRect(
                x: newX,
                y: newY,
                width: newWidht,
                height: newHeight
            )

            CATransaction.begin()
            CATransaction.setCompletionBlock { [weak self] in
                self?.animateInfoLayer()
            }
            textContentLayer.add(animation, forKey: "secondAnimationKey")
            CATransaction.commit()
        }

        animatePinkLayer()
        animateContentTextLayer()
    }

    func animateTitleLayer() {
        textContentLayer.addSublayer(titleTextLayer)
        let fontSize = defaultTextContentLayerHeight / 2
        let font = UIFont.italicSystemFont(ofSize: fontSize)
        let newY = (font.ascender + font.descender + font.leading) / 2
        titleTextLayer.frame = CGRect(x: 0, y: newY, width: textContentLayer.bounds.width, height: defaultTextContentLayerHeight - newY)
        titleTextLayer.string = "NEW POST"
        titleTextLayer.foregroundColor = UIColor.black.cgColor
        titleTextLayer.alignmentMode = .center
        titleTextLayer.font = font
        titleTextLayer.fontSize = fontSize
    }

    func animateInfoLayer() {
        pinkLayer.addSublayer(infoTextLayer)
        let fontSize = (sprawlingPinkLayerHeight - defaultTextContentLayerHeight)
        let font = UIFont.italicSystemFont(ofSize: fontSize)
        infoTextLayer.frame = CGRect(
            x: textContentLayer.frame.minX,
            y: defaultTextContentLayerHeight - defaultInset / 2,
            width: textContentLayer.bounds.width,
            height: fontSize
        )
        infoTextLayer.string = "SWIPE UP TO READ"
        infoTextLayer.foregroundColor = UIColor.black.cgColor
        infoTextLayer.alignmentMode = .center
        infoTextLayer.font = font
        infoTextLayer.fontSize = fontSize
    }

    // MARK: Setup layers

    private func setupSubLayers() {
        prepareWhiteLayer()
        preparePinkLayer()
    }

    private func prepareWhiteLayer() {
        addSublayer(whiteLayer)
        whiteLayer.backgroundColor = UIColor.white.cgColor
        whiteLayer.anchorPoint = .zero
    }

    private func preparePinkLayer() {
        whiteLayer.addSublayer(pinkLayer)
        pinkLayer.backgroundColor = UIColor(red: 255 / 255, green: 203 / 255, blue: 219 / 255, alpha: 1).cgColor
        pinkLayer.anchorPoint = .zero
    }
}
