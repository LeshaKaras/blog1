import UIKit

class TopLayer: CALayer, AnimateLayerProtocol {
    let imageLayerMask = CALayer()
    let imageLayer: CALayer = {
        let layer = CALayer()
        layer.contentsScale = UIScreen.main.scale
        return layer
    }()

    let defaultInset: CGFloat
    let timings: TopLayerAnimationTimings

    init(
        defaultInset: CGFloat,
        timings: TopLayerAnimationTimings
    ) {
        self.defaultInset = defaultInset
        self.timings = timings
        super.init()
        setupLayers()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupFrame(cgRect: CGRect) {
        frame = cgRect

        imageLayer.frame = bounds
        imageLayerMask.frame = CGRect(
            x: bounds.minX,
            y: bounds.maxY,
            width: imageLayer.bounds.width,
            height: 0
        )

        imageLayer.contents = girlImage.resizeImage(targetSize: SizeController.originalSize).cgImage
    }

    func animate() {
        let maskHeightAnimation = CABasicAnimation.transfromLayerBounds(
            imageLayerMask,
            newHeight: bounds.height - defaultInset,
            duration: timings.deploymentDuration,
            delay: 0
        )
        imageLayerMask.add(maskHeightAnimation, forKey: "bounds")

        let scaleAnimation = CABasicAnimation.layerScaleAnimation(
            duration: timings.zoomDuration,
            fromValue: timings.zoomSetting.fromValue,
            toValue: timings.zoomSetting.toValue
        )
        imageLayer.add(scaleAnimation, forKey: "transform.scale")
    }

    private func setupLayers() {
        addSublayer(imageLayer)
        imageLayer.backgroundColor = UIColor.white.cgColor
        imageLayer.isGeometryFlipped = true

        mask = imageLayerMask
        imageLayerMask.backgroundColor = UIColor.red.cgColor
        imageLayerMask.masksToBounds = true
        imageLayerMask.anchorPoint = CGPoint(x: 0, y: 1)
    }
}
