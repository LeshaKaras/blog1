import UIKit

struct DefaultSetupModel: SetupAnimationLayerProtocol {
    let defaultInset: CGFloat
    let topTimings: TopLayerAnimationTimings
    let bottomTimings: BottomLayerAnimationTimings
}

struct TopLayerAnimationTimings {
    let deploymentDuration: Double
    let zoomDuration: Double
    let zoomSetting: ZoomSettingModel
}

struct ZoomSettingModel {
    var fromValue: CGFloat
    var toValue: CGFloat
}

struct BottomLayerAnimationTimings {
    let firstStageDuration: Double
    let secondStageDuration: Double
}
