import UIKit

protocol AnimateLayerProtocol: CALayer {
    func setupFrame(cgRect: CGRect)
    func animate()
}
