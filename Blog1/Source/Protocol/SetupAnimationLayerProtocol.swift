import UIKit

protocol SetupAnimationLayerProtocol {
    var defaultInset: CGFloat { get }
    var topTimings: TopLayerAnimationTimings { get }
    var bottomTimings: BottomLayerAnimationTimings { get }
}
