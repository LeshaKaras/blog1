import SnapKit
import UIKit

extension UIView {
    func addSubview(_ view: UIView, _ closure: (ConstraintMaker) -> Void) {
        addSubview(view)
        view.snp.makeConstraints(closure)
    }

    func makeConstraints(_ closure: (ConstraintMaker) -> Void) {
        snp.makeConstraints(closure)
    }

    func remakeConstraints(_ closure: (ConstraintMaker) -> Void) {
        snp.remakeConstraints(closure)
    }

    func updateConstraints(_ closure: (ConstraintMaker) -> Void) {
        snp.updateConstraints(closure)
    }

    func removeConstraints() {
        snp.removeConstraints()
    }
}
