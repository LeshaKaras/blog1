import UIKit

extension CABasicAnimation {
    class func transfromLayerBounds(
        _ layer: CALayer,
        newWidht: CGFloat? = nil,
        newHeight: CGFloat? = nil,
        duration: Double,
        delay: Double
    ) -> CABasicAnimation {
        let check = newWidht == nil && newHeight == nil
        assert(!check, "Height or width must be changed")
        let newBounds = CGRect(
            x: layer.bounds.minX,
            y: layer.bounds.minY,
            width: newWidht != nil ? newWidht! : layer.bounds.width,
            height: newHeight != nil ? newHeight! : layer.bounds.height
        )
        let animation = CABasicAnimation(keyPath: "bounds")
        animation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        animation.duration = CFTimeInterval() + duration
        animation.beginTime = CACurrentMediaTime() + delay
        animation.fromValue = NSValue(cgRect: layer.bounds)
        animation.toValue = NSValue(cgRect: newBounds)
        animation.fillMode = .both
        animation.isRemovedOnCompletion = false
        return animation
    }

    class func layerScaleAnimation(duration: CFTimeInterval, fromValue: CGFloat, toValue: CGFloat) -> CABasicAnimation {
        let scaleAnimation: CABasicAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.fromValue = fromValue
        scaleAnimation.toValue = toValue
        scaleAnimation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        scaleAnimation.duration = CFTimeInterval() + duration
        return scaleAnimation
    }
}
